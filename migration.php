<?php
namespace craft\contentmigrations;

use craft\db\Migration;
use dgrigg\migrationassistant\MigrationAssistant;

/**
 * Starter Migration Template
 *
 * Only migrate once when you init the project.
 * You can delete this file after migration
 * but make sure you comment out line 9 of Dockerfile before deleting
 */
class m190611_101013_migration extends Migration
{
    /**
    Migration manifest:

    FIELD
        - body
        - h1Title
        - h2SubTitle
        - seoDescription
        - seoImage
        - seoTitle

    SECTION
        - homepage
        - styleguide

    ASSETVOLUME
        - local

    GLOBAL
        - defaultSeo
            */

private $json = <<<'JSON'
{"settings":{"dependencies":{"sections":[{"name":"Homepage","handle":"homepage","type":"single","enableVersioning":"1","propagateEntries":"1","sites":{"default":{"site":"default","hasUrls":"1","uriFormat":"__home__","enabledByDefault":"1","template":"pages/homepage"}},"entrytypes":[{"sectionHandle":"homepage","hasTitleField":"0","titleLabel":"","titleFormat":"{section.name|raw}","name":"Homepage","handle":"homepage","fieldLayout":{"General":["h1Title","h2SubTitle","body"]},"requiredFields":[]}]},{"name":"Styleguide","handle":"styleguide","type":"single","enableVersioning":"1","propagateEntries":"1","sites":{"default":{"site":"default","hasUrls":"1","uriFormat":"styleguide","enabledByDefault":"1","template":"pages/styleguide"}},"entrytypes":[{"sectionHandle":"styleguide","hasTitleField":"0","titleLabel":null,"titleFormat":"{section.name|raw}","name":"Styleguide","handle":"styleguide","fieldLayout":[],"requiredFields":[]}]}],"assetVolumes":[{"name":"Local (please delete once you setup S3)","handle":"local","type":"craft\\volumes\\Local","sortOrder":"1","typesettings":{"path":"@storage/assets"}}]},"elements":{"fields":[{"group":"Common","name":"Body","handle":"body","instructions":"","translationMethod":"none","translationKeyFormat":null,"required":null,"searchable":"1","type":"craft\\fields\\PlainText","typesettings":{"placeholder":"","code":"","multiline":"1","initialRows":"4","charLimit":"","columnType":"text"}},{"group":"Common","name":"H1 Title","handle":"h1Title","instructions":"","translationMethod":"none","translationKeyFormat":null,"required":null,"searchable":"1","type":"craft\\fields\\PlainText","typesettings":{"placeholder":"","code":"","multiline":"","initialRows":"4","charLimit":"","columnType":"text"}},{"group":"Common","name":"H2 Sub-Title","handle":"h2SubTitle","instructions":"","translationMethod":"none","translationKeyFormat":null,"required":null,"searchable":"1","type":"craft\\fields\\PlainText","typesettings":{"placeholder":"","code":"","multiline":"1","initialRows":"2","charLimit":"","columnType":"text"}},{"group":"SEO","name":"SEO Description","handle":"seoDescription","instructions":"","translationMethod":"none","translationKeyFormat":null,"required":null,"searchable":"1","type":"craft\\fields\\PlainText","typesettings":{"placeholder":"","code":"","multiline":"1","initialRows":"2","charLimit":"","columnType":"text"}},{"group":"SEO","name":"SEO Image","handle":"seoImage","instructions":"","translationMethod":"site","translationKeyFormat":null,"required":null,"searchable":"1","type":"craft\\fields\\Assets","typesettings":{"useSingleFolder":"","defaultUploadLocationSource":"local","defaultUploadLocationSubpath":"","singleUploadLocationSource":"local","singleUploadLocationSubpath":"","restrictFiles":"","allowedKinds":null,"sources":[],"source":null,"targetSiteId":null,"viewMode":"list","limit":"1","selectionLabel":"","localizeRelations":false}},{"group":"SEO","name":"SEO Title","handle":"seoTitle","instructions":"","translationMethod":"none","translationKeyFormat":null,"required":null,"searchable":"1","type":"craft\\fields\\PlainText","typesettings":{"placeholder":"","code":"","multiline":"","initialRows":"4","charLimit":"","columnType":"text"}}],"sections":[{"name":"Homepage","handle":"homepage","type":"single","enableVersioning":"1","propagateEntries":"1","sites":{"default":{"site":"default","hasUrls":"1","uriFormat":"__home__","enabledByDefault":"1","template":"pages/homepage"}},"entrytypes":[{"sectionHandle":"homepage","hasTitleField":"0","titleLabel":"","titleFormat":"{section.name|raw}","name":"Homepage","handle":"homepage","fieldLayout":{"General":["h1Title","h2SubTitle","body"]},"requiredFields":[]}]},{"name":"Styleguide","handle":"styleguide","type":"single","enableVersioning":"1","propagateEntries":"1","sites":{"default":{"site":"default","hasUrls":"1","uriFormat":"styleguide","enabledByDefault":"1","template":"pages/styleguide"}},"entrytypes":[{"sectionHandle":"styleguide","hasTitleField":"0","titleLabel":null,"titleFormat":"{section.name|raw}","name":"Styleguide","handle":"styleguide","fieldLayout":[],"requiredFields":[]}]}],"assetVolumes":[{"name":"Local (please delete once you setup S3)","handle":"local","type":"craft\\volumes\\Local","sortOrder":"1","typesettings":{"path":"@storage/assets"},"fieldLayout":[]}],"globals":[{"name":"Default SEO","handle":"defaultSeo","fieldLayout":{"Tab 1":["seoTitle","seoDescription","seoImage"]},"requiredFields":[]}]}}}
JSON;

    /**
     * Any migration code in here is wrapped inside of a transaction.
     * Returning false will rollback the migration
     *
     * @return bool
     */
    public function safeUp()
    {
        return MigrationAssistant::getInstance()->migrations->import($this->json);
    }

    public function safeDown()
    {
        echo "starterMigration cannot be reverted.\n";
        return false;
    }
}
