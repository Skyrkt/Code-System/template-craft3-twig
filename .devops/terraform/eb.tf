resource "aws_elastic_beanstalk_environment" "production" {
  name          = "${var.application_info["name_lower"]}-production"
  application   = "${aws_elastic_beanstalk_application.app.name}"
  template_name = "${aws_elastic_beanstalk_configuration_template.single_docker_template.name}"
  tier          = "WebServer"
  cname_prefix  = "${var.application_info["name_lower"]}-production"

  setting {
    namespace = "aws:elasticbeanstalk:application:environment"
    name      = "DB_SERVER"
    value     = "${aws_db_instance.production.address}"
  }

  setting {
    namespace = "aws:elasticbeanstalk:application:environment"
    name      = "DB_PASSWORD"
    value     = "${var.db_password["production"]}"
  }

  setting {
    namespace = "aws:elasticbeanstalk:application:environment"
    name      = "DB_USER"
    value     = "${var.application_info["name_lower"]}"
  }

  setting {
    namespace = "aws:elasticbeanstalk:application:environment"
    name      = "DB_DATABASE"
    value     = "${var.application_info["name_lower"]}_production"
  }

  setting {
    namespace = "aws:elasticbeanstalk:application:environment"
    name      = "ENVIRONMENT"
    value     = "production"
  }
}

resource "aws_elastic_beanstalk_environment" "staging" {
  name          = "${var.application_info["name_lower"]}-staging"
  application   = "${aws_elastic_beanstalk_application.app.name}"
  template_name = "${aws_elastic_beanstalk_configuration_template.single_docker_template.name}"
  tier          = "WebServer"
  cname_prefix  = "${var.application_info["name_lower"]}-staging"

  setting {
    namespace = "aws:elasticbeanstalk:application:environment"
    name      = "DB_SERVER"
    value     = "${aws_db_instance.staging.address}"
  }

  setting {
    namespace = "aws:elasticbeanstalk:application:environment"
    name      = "DB_PASSWORD"
    value     = "${var.db_password["staging"]}"
  }

  setting {
    namespace = "aws:elasticbeanstalk:application:environment"
    name      = "DB_USER"
    value     = "${var.application_info["name_lower"]}"
  }

  setting {
    namespace = "aws:elasticbeanstalk:application:environment"
    name      = "DB_DATABASE"
    value     = "${var.application_info["name_lower"]}_staging"
  }

  setting {
    namespace = "aws:elasticbeanstalk:application:environment"
    name      = "ENVIRONMENT"
    value     = "staging"
  }
}

resource "aws_elastic_beanstalk_environment" "qa" {
  name          = "${var.application_info["name_lower"]}-qa"
  application   = "${aws_elastic_beanstalk_application.app.name}"
  template_name = "${aws_elastic_beanstalk_configuration_template.single_docker_template.name}"
  tier          = "WebServer"
  cname_prefix  = "${var.application_info["name_lower"]}-qa"

  setting {
    namespace = "aws:elasticbeanstalk:application:environment"
    name      = "DB_SERVER"
    value     = "${aws_db_instance.qa.address}"
  }

  setting {
    namespace = "aws:elasticbeanstalk:application:environment"
    name      = "DB_PASSWORD"
    value     = "${var.db_password["qa"]}"
  }

  setting {
    namespace = "aws:elasticbeanstalk:application:environment"
    name      = "DB_USER"
    value     = "${var.application_info["name_lower"]}"
  }

  setting {
    namespace = "aws:elasticbeanstalk:application:environment"
    name      = "DB_DATABASE"
    value     = "${var.application_info["name_lower"]}_qa"
  }

  setting {
    namespace = "aws:elasticbeanstalk:application:environment"
    name      = "ENVIRONMENT"
    value     = "qa"
  }
}
