<?php
/**
 * General Configuration
 *
 * All of your system's general configuration settings go in here. You can see a
 * list of the available settings in vendor/craftcms/cms/src/config/GeneralConfig.php.
 *
 * @see \craft\config\GeneralConfig
 */

return [
    // Global settings
    '*' => [
        'defaultWeekStartDay' => 1,
        'omitScriptNameInUrls' => true,
        'cpTrigger' => 'admin',
        'securityKey' => getenv('SECURITY_KEY'),
        'useProjectConfigFile' => true,
        'allowUpdates' => false,
        'siteUrl' => 'https://{{cli_domain}}'
    ],

    // Dev environment settings
    'dev' => [
        'devMode' => true,
        'allowUpdates' => true,
        'siteUrl' => '{{cli_projectNameLower}}.docker'
    ],

    // QA environment settings
    'qa' => [
        'devMode' => true,
        'allowAdminChanges' => true,
        'siteUrl' => 'https://qa.{{cli_domain}}'
    ],

    // Staging environment settings
    'staging' => [
        'allowAdminChanges' => true,
        'siteUrl' => 'https://staging.{{cli_domain}}'
    ],

    // Production environment settings
    'production' => [
        'allowAdminChanges' => true,
        'siteUrl' => 'https://{{cli_domain}}'
    ],
];

